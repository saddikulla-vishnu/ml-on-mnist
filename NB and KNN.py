import pandas
import sys
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

filename = 'Datasets/mnist_train.csv'
trainDataset = pandas.read_csv(filename,skipinitialspace=True)

filename = 'Datasets/mnist_test.csv'
testDataset = pandas.read_csv(filename,skipinitialspace=True)

array = trainDataset.values
XTrain = array[:,1:785]
YTrain = array[:,0]

array = testDataset.values
XTest = array[:,1:785]
YTest = array[:,0]

nb = GaussianNB()

# Train the model using the training sets
nb.fit(XTrain,YTrain)

# Predict Output
predicted= nb.predict(XTest)

print(accuracy_score(YTest, predicted))
# print(confusion_matrix(YTest, predicted))
# print(classification_report(YTest, predicted))

# print("KNNNN")
# knn = KNeighborsClassifier()
# knn.fit(XTrain,YTrain)
# predictions = knn.predict(XTest)
# print(accuracy_score(YTest, predictions, normalize=False))

print("in the end")
